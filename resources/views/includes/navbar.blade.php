<!--Navbar-->
<div class="container"><!--(.container)-->
    <nav class="row navbar navbar-expand-lg navbar-light bg-white"> <!--tag nav(nav), ada menggunakan beberapa class didalamnya. class row(.row): if bikin 1 div di dalam container, biasanya akan di taro di satu row. class navbar(.navbar): bisa liat docs bootstrap. navbar-expand-lg: kalo di layar gede, jadi auto panjang)-->
        <a href="{{ url('/') }}" class="navbar-brand"><!--tag link(a). class navbar-brand(.navbar-brand)-->
            <img src="{{ url('frontend/images/logo.png') }}" alt="Logo Nomads">
        </a>
        <button class="navbar-toggler navbar-toggler-right" 
        type="button" 
        data-toggle="collapse" 
        data-target="#navb"
        >
        <span class="navbar-toggler-icon"></span>
    </button><!--tag button(button). (.navbar-toggler): toggler navbar pas responsive. (.navbar-toggler-right): biar posisi di kanan(liat docs).-->
    
    <div class="collapse navbar-collapse" id="navb"><!--(.collapse): link2 pada menu biar bisa tampil jg di responsive. (.navbar-collapse): yg mau di kecilin menu yang link-->
        <ul class="navbar-nav ml-auto mr-3"><!--ml = margin left auto, sebelah kiri menu nya langsung di kosongin dengan margin row nya 3 -->
            <li class="nav-item mx-md-2"><a href="#" class="nav-link active">Home</a></li><!--tag (li). class(.nav-item). mx: margin X(kordinat x, kiri & kanan). 2: satuan ukuran dari bootstrap. bikin tag tapi mau langsung ada isi di dalamnya, gunakan >a-->
            <li class="nav-item mx-md-2"><a href="#" class="nav-link">Paket Travel</a></li>
            <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown">Service</a>
                <div class="dropdown-menu">
                <a href="#" class="dropdown-item">Link</a>
                <a href="#" class="dropdown-item">Link</a>
                <a href="#" class="dropdown-item">Link</a>
            </div>
            </li>
            <li class="nav-item mx-md-2"><a href="#" class="nav-link">Testimonial</a></li>
        </ul>

        @guest
            <!-- Mobile Button -->
            <form class="form-inline d-sm-block d-md-none"><!-- d-sm-block: agar tampilan sm (layar kecil) kalo di buat kotak, bentuk nya dari pojok kiri sampe pojok kanan. Untuk d-md-non, untuk layar tablet diatasnya, kita tidak munculin ini (hanya muncul di mobile doang) -->
                <button class="btn btn-login my-2 my-sm-0" type="button" onclick="event.preventDefault(); location.href='{{ url('login') }}';">Masuk</button><!-- my: margin kordinat y (jarak atas-bawah). my-sm-0: kordinat y (atas) tapi ga dikasih jarak di sm (0). -->
            </form>

            <!-- Desktop Button -->
            <form class="form-inline my-2 my-lg-0 d-none d-md-block"> <!-- my-2: kordinat y kasih jarak 2. my-lg: layar gede kordinat y tidak di kasih jarak (0). d-none: tidak di munculin, sampai nanti di munculin nya di d-md-block (kebalikan mobile button) -->
                <button class="btn btn-login btn-navbar-right my-2 my-sm-0 px-4" type="button" onclick="event.preventDefault(); location.href='{{ url('login') }}';">Masuk</button><!-- px-4: padding di kordinat x(kiri-kanan) akan kasih jarak nya 4 -->
            </form>
        @endguest

        @auth
            <!-- Mobile Button -->
            <form class="form-inline d-sm-block d-md-none" action="{{ url('logout') }}" method="POST"><!-- d-sm-block: agar tampilan sm (layar kecil) kalo di buat kotak, bentuk nya dari pojok kiri sampe pojok kanan. Untuk d-md-non, untuk layar tablet diatasnya, kita tidak munculin ini (hanya muncul di mobile doang) -->
                @csrf
                <button class="btn btn-login my-2 my-sm-0" type="submit">Keluar</button><!-- my: margin kordinat y (jarak atas-bawah). my-sm-0: kordinat y (atas) tapi ga dikasih jarak di sm (0). -->
            </form>

            <!-- Desktop Button -->
            <form class="form-inline my-2 my-lg-0 d-none d-md-block" action="{{ url('logout') }}" method="POST"> <!-- my-2: kordinat y kasih jarak 2. my-lg: layar gede kordinat y tidak di kasih jarak (0). d-none: tidak di munculin, sampai nanti di munculin nya di d-md-block (kebalikan mobile button) -->
                @csrf
                <button class="btn btn-login btn-navbar-right my-2 my-sm-0 px-4" type="submit">Keluar</button><!-- px-4: padding di kordinat x(kiri-kanan) akan kasih jarak nya 4 -->
            </form>
        @endauth

    </div>  
    </nav> 
</div>