<!--Navbar-->
<div class="container"><!--(.container)-->
    <nav class="row navbar navbar-expand-lg navbar-light bg-white"> <!--tag nav(nav), ada menggunakan beberapa class didalamnya. class row(.row): if bikin 1 div di dalam container, biasanya akan di taro di satu row. class navbar(.navbar): bisa liat docs bootstrap. navbar-expand-lg: kalo di layar gede, jadi auto panjang)-->
        <div class="navbar-nav ml-auto mr-auto mr-sm-auto mr-lg-0 mr-md-auto">
            <a href="{{ url('/') }}" class="navbar-brand">
                <img src="{{ url('frontend/images/logo.png') }}" alt="">
            </a>
        </div>
        <ul class="navbar-nav mr-auto d-none d-lg-block"><!--d-none: kalau di mobile ga di munculin-->
            <li>
                <span class="text-muted">
                    | &nbsp; Beyond the explorer of the world
                </span>
            </li>
        </ul>
    </nav> 
</div>